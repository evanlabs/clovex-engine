-- dependences
luatable = require "luatable"
matrix = require "matrix"
xml = require "xmlparser"

-- editor things
camera = {x = 0, y = 0}

-- this functons for creating entities

----------------------------
--- start component functions
----------------------------

-- this function creates and inits an entity from given template path
function createEntity(path) 
	print("Initializing entity '"..path.."'...")
	path = path or 'empty.entity'
	local content = love.filesystem.read(path)
	local luaEntity = assert(loadstring(content))()
	local res = initEntity(luaEntity)
	print("Done.")
	return res
end

-- this function creates and inits an entity from given XML path (beta)
function createEntityFromXML(path) 
	print("Initializing entity '"..path.."'(from XML)...")
	path = path or 'empty.xentity'
	local content = love.filesystem.read(path)
	local xml = require("xmlparser").newParser()
	xml = xml:ParseXmlText(content)
	local luaEntity = xmlToLua(xml.entity);
	local res = initEntity(luaEntity)
	print("Done.")
	return res
end

-- initialise loaded entity
function initEntity(entity)
	local finalEntity = {}
	finalEntity.getComponent = getComponent
	finalEntity.addComponent = addComponent
	finalEntity.addChild = addChild
	finalEntity.children = {}	
	finalEntity.components = {}	
	for k, v in pairs(entity.components) do
		-- add this component into list
		local test = finalEntity:addComponent(v.name, v.script, v.properties)
		--print(print_r(finalEntity.components[#finalEntity.components].position))
		--print("Engine: "..print_r(test["position"]))
	end
	-- do same init things for children (nested entities)
	if entity.children ~= nil then
		for k, v in pairs(entity.children) do
			finalEntity.children[k] = initEntity(v)
		end
	end

	return finalEntity
end

----------------------------
--- end component functions
----------------------------

-- call this for control root entity (or any of the entity and his children)
-- this is also callback wrappers

----------------------------
--- start callback wrappers
----------------------------
function keyPressedEntity(entity, key) 
	for k, v in pairs(entity.components) do
		print(type(v.disabled))
		if v.disabled ~= "true" then
			v.keypressed(key)
		end
	end
	for k, v in pairs(entity.children) do
		keyPressedEntity(v, key)
	end	
end
function keyReleasedEntity(entity, key) 
	for k, v in pairs(entity.components) do
		print(type(v.disabled))
		if v.disabled ~= "true" then
			v.keyreleased(key)
		end
	end
	for k, v in pairs(entity.children) do
		keyReleasedEntity(v, key)
	end	
end
function updateEntity(entity, dt) 
	for k, v in pairs(entity.components) do
		print(type(v.disabled))
		if v.disabled ~= "true" then
			v.update(dt)
		end
	end
	for k, v in pairs(entity.children) do
		updateEntity(v, dt)
	end	
end
function beforeDrawEntity(entity) 
	for k, v in pairs(entity.components) do
		if v.disabled ~= "true" then
			v.beforeDraw()
		end
	end
end
function drawEntity(entity) 
	beforeDrawEntity(entity)
	for k, v in pairs(entity.components) do
		if v.disabled ~= "true" then
			v.draw()
		end
	end
	for k, v in pairs(entity.children) do
		drawEntity(v)
	end	
	for k, v in pairs(entity.components) do
	end
	afterDrawEntity(entity)
end
function afterDrawEntity(entity) 
	for k, v in pairs(entity.components) do
		if v.disabled ~= "true" then
			v.afterDraw()
		end
	end
end

----------------------------
--- end callback wrappers
----------------------------

-- this functions for working with components

----------------------------
--- start Entity functions
----------------------------

--[[function Entity(name, parent) 
	local newEntity = {
		name = name or "" 
	} 
	if (parent ~= nil) then
		parent:addChild(initEntity(newEntity))
		return parent.children[#parent.children]
	end
	return initEntity(newEntity)
end]]

-- adds component from script at given path
function addComponent(self, name, path, prop) 
	local newComponent = getFreshEnv(self)
	newComponent._getters = {}
	newComponent._setters = {}
	local mt = {
		__newindex = function(table, key, value)
			-- for debug
			--print("Set call for table ["..k.."] = "..tostring(value))
			if table == newComponent and type(value) == "function" then
				addValueEnv(table, key, value, table)
			elseif (table._setters[key] ~= nil) then
				table._setters[key](value)
			else
				rawset(table, key, value)
			end
		end,
		__index = function(table, key)
			-- for debug
			--print("Get call for table ["..key.."]")
			if (key ~= "_getters" and newComponent._getters[key] ~= nil) then
				return newComponent._getters[key]()
			else
				return rawget(newComponent, key)
			end
		end
	}
	setmetatable(newComponent, mt)
	newComponent = setupNewFunctions(newComponent, newComponent)
	local content = love.filesystem.read(path)
	local worker = assert(loadstring(content, path))
	setfenv(worker, newComponent)
	worker()

	if prop ~= nil then
		for _, v in pairs(prop) do
			newComponent[v.key] = v.value
		end
	end

	newComponent.name = name
	newComponent:load()

	table.insert(self.components, newComponent)
	return self.components[#self.components]
end

-- get specified component from entity's component
-- returns first component named 'name' or false
function getComponent(self, name) 
	for _, v in pairs(self.components) do
		if v.script == name or v.name == name then
			return v
		end
	end
	return false
end

-- adds entity as a child to another entity
function addChild(self, child)
	self.children[#self.children + 1] = child
	return self.children[#self.children]
end

----------------------------
--- end Entity functions
----------------------------

-- service functions for creatings sandboxes
----------------------------
--- start service functions
----------------------------

-- creates fresh environment for component
function getFreshEnv(entity)
	return {
		loadstring = loadstring,
		assert = assert,
		print = print,
		setfenv = setfenv,
		pairs = pairs,
		ipairs = ipairs,
		error = error,
		type = type,
		print_r = print_r,
		love = love,
		math = math,
		camera = camera,
		matrix = matrix,
		addComponent = addComponent,
		getComponent = getComponent,
		addChild = addChild,
		entity = entity,
		getFreshEnv = getFreshEnv,
		tconcat = tconcat,
		table = table,
		require = require,
		tonumber = tonumber,
		tostring = tostring
	}
end

-- for each function we will set current component env
-- this is for remove needing in using 'self'
function addValueEnv(t, k, v)
	rawset(t, k, v)
	setfenv(t[k], t)
end

-- setup functions for fresh env
function setupNewFunctions(to, env)
	to._G = env
	to.property = function(name, defaultValue, set, get)
		if (set == nil and get == nil) then
			_G[name] = defaultValue
		end
		_setters[name] = set
		_getters[name] = get
	end
	-- import another component into this
	to.import = function(path, rewriteCurrent)
		rewriteCurrent = rewriteCurrent or true
		local content = love.filesystem.read(path)
		local worker = assert(loadstring(content))
		local comp = {}
		setfenv(worker, comp)
		worker()
		if rewriteCurrent then
			for k, v in pairs(comp) do
				_G[k] = v
			end
		end
		return tconcat(comp, getFreshEnv(entity))
	end

	to.load = function() end
	to.update = function() end
	to.keypressed = function() end
	to.keyreleased = function() end
	to.keyreleased = function() end
	to.draw = function() end
	to.beforeDraw = function() end
	to.afterDraw = function() end

	to.blabla = "zaz";


	-- editor submodule
	to.editor = {
		redraw = function() end,
		update = function() end
	}
	return to
end

-- concats two table variables
function tconcat(t1,t2)
    for k, v in pairs(t2) do
        t1[k] = v
    end
    return t1
end

-- converts XML entity object t Lua Table object
function xmlToLua(xml)
	local entity = {components = {}, children = {}}
	entity.name = xml["@name"] or "untitled"
	if (entity.src ~= nil) then
		xml = createEntityFromXML(entity.src);
	end
	for k, v in pairs(xml:getChildren()) do
		local nodeName = v:getName()
		-- to parse scripts
		if (nodeName == "script") then
			local newScript = {properties = {}}
			newScript.script = v["@src"]
			newScript.name = v["@id"]
			for k, v2 in pairs(v:getProperties()) do
				if (v2.name ~= "id" and v2.name ~= "src") then
					table.insert(newScript.properties, {key = v2.name, value = v2.value})
				end
			end
			table.insert(entity.components, newScript)
		end
	end
	-- to parse children
	if (xml.children ~= nil) then
		for k, v2 in pairs(xml.children:getChildren()) do
			table.insert(entity.children, xmlToLua(v2))
		end
	end
	return entity
end

----------------------------
--- end service functions
----------------------------

-- debug functions

----------------------------
--- start debug functions
----------------------------

function print_r ( t )  
	local result = ""
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            result = result .. "\n" .. indent.."*"..tostring(t)
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        result = result .. "\n" .. indent.."["..pos.."] => "..tostring(t).." {"
                        sub_print_r(val,indent.." ")
                        result = result .. "\n" .. indent..string.rep(" ",string.len(pos)+3).."}"
                    elseif (type(val)=="string") then
                        result = result .. "\n" .. indent.."["..pos..'] => "'..val..'"'
                    else
                        result = result .. "\n" .. indent.."["..pos.."] => "..tostring(val)
                    end
                end
            else
                result = result .. "\n" .. indent..tostring(t)
            end
        end
    end
    if (type(t)=="table") then
        result = result .. "\n" .. tostring(t).." {"
        sub_print_r(t,"  ")
        result = result .. "\n" .. "}"
    else
        sub_print_r(t," ")
    end
   	return result
end

----------------------------
--- end debug functions
----------------------------