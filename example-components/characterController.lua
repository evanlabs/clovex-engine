property("maxSpeed", 1, 
	function(value) 
		_maxSpeed = value 
		print("Set for maxSpeed " .. value)
	end,
	function() 
		print("Get for maxSpeed " .. _maxSpeed)
		return _maxSpeed
	end
)
property("acceleration", 0.1)

function load() 
	transform = entity:getComponent("Transform")
	local a = 5
end

function update(dt)
	vspeed = 0
	hspeed = 0
	if (love.keyboard.isDown("w")) then
		vspeed = -maxSpeed
	end
	if (love.keyboard.isDown("s")) then
		vspeed = maxSpeed
	end
	if (love.keyboard.isDown("a")) then
		hspeed = -maxSpeed
	end
	if (love.keyboard.isDown("d")) then
		hspeed = maxSpeed
	end
	transform.translate(hspeed, vspeed)
end
