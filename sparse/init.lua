-- module to parsing complex XML attributes into Lua structure
local str = require "string_splitjoin"
local sparse = {}

function sparse.toNumberArray(data)
	local parsed = str.split(data, ':')
	for k, v in ipairs(parsed) do
		parsed[k] = tonumber(v)
	end
	return parsed
end

return sparse