shape = "circle" -- circle, rectangle
drawMode = "fill" -- fill, line
size = {x = 100, y = 100, r = 50}	-- r for circle
color = {255, 255, 255, 255}

function draw()
	local r, g, b, a = love.graphics.getColor()
	love.graphics.setColor(color[1], color[2], color[3], color[4] or 255)
	if shape == "circle" then
		love.graphics.circle(drawMode, 0, 0, size.r)
	elseif shape == "rectangle" then
		love.graphics.rectangle(drawMode, 0, 0, size.x, size.y)
	end
	love.graphics.setColor(r, g, b, a)
end