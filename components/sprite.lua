property("path", "")
property("offset", {x = 0, y = 0})
property("color", {255, 255, 255, 255})
property("bounds", nil)

parser = require "sparse"

function load()

	if path ~= "" then
		image = love.graphics.newImage(path)
	end
	if (bounds == nil and image ~= nil) then
		setBounds(0, 0, image:getWidth(), image:getHeight())
	end
end

function setBounds(x, y, w, h) 
	bounds = love.graphics.newQuad(x, y, w, h, image:getDimensions())
end

function draw()
	if image ~= nil then
		local r, g, b, a = love.graphics.getColor()
		love.graphics.setColor(color[1], color[2], color[3], color[4] or 255)
		love.graphics.draw(image, bounds, 0, 0, 0, 1, 1, offset.x, offset.y)
		love.graphics.setColor(r, g, b, a)
	end
end