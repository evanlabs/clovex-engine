-- Transformation properties
property("position", {x = 0, y = 0})
rotation = 0
property("scale", {x = 1, y = 1})

parser = require "sparse"

function parseStringTransform() 
	if (type(position) == "string") then 
		local parsed = parser.toNumberArray(position, ':')
		position = {x = tonumber(parsed[1]), y = tonumber(parsed[2])}
	end
	if (type(rotation) == "string") then 
		rotation = tonumber(rotation)
	end
	if (type(scale) == "string") then 
		local parsed = parser.toNumberArray(scale, ':')
		if (#parsed == 1) then
			scale = {x = tonumber(scale), y = tonumber(scale)}
		else 
			scale = {x = tonumber(parsed[1]), y = tonumber(parsed[2])}
		end
	end
end

function load()
	parseStringTransform()
end

function update()
end

function beforeDraw()
	matrix.translate(position.x, position.y)
	matrix.rotate(rotation/180*math.pi)
	matrix.scale(scale.x, scale.y)
end

function afterDraw()
	matrix.translate(-position.x, -position.y)
	matrix.rotate(-rotation/180*math.pi)
	matrix.scale(1/scale.x, 1/scale.y)
end

-- Updates global position (call before update)
function recalculateTransformation()
	globalPosition = {}
	--globalPosition.x, globalPosition.y = matrix.xform(currMatrix, 0, 0)
end

function translate(x, y)
	if (x ~= nil) then position.x = position.x + x end
	if (y ~= nil) then position.y = position.y + y end
end

function rotate(angle)
	if (angle ~= nil) then rotation = rotation + angle end
end